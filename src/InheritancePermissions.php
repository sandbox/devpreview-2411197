<?php

/**
 * @file
 * Contains \Drupal\role_inherit\InheritancePermissions.
 */

namespace Drupal\role_inherit;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a class containing permission callbacks.
 */
class InheritancePermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of inheritance permissions.
   *
   * @return array
   */
  public function inheritPermissions() {
    $roles = role_inherit_user_role_names();
    $permissions = array();
    foreach ($roles as $role_id => $role) {
      $permissions[ROLE_INHERIT_PERMISSION_PREFIX . $role_id] = array(
        'title' => $this->t('Inherit %role role', array('%role' => $role)),
        'description' => $this->t('Inheritance %role role permissions', array('%role' => $role))
      );
    }
    return $permissions;
  }

}
